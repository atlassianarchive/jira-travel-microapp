define ->
  
  # used inside template to assign button class based on text value
  window.statusButton = (status) ->
    switch status
      when 'Open' then 'btn-info'
      when 'Approved' then 'btn-success'
      when 'Denied' then 'btn-danger'
      when 'Booked' then 'btn-inverse'

  # JSONP handler for exchange rate conversion
  window.parseExchangeRate = (data) ->
    name = data.query.results.row.name
    rate = parseFloat data.query.results.row.rate, 10
    $('#costin').html (parseFloat $('#cost').val() * rate)
      .round(2)
      .format(2) + 
      " AUD"

  # JSONP handler for place name lookup
  window.handlePlaceJSONP = (d) ->
    window.typeahead.process _.map d, (place)->
      place.replace /NS/, "NSW"