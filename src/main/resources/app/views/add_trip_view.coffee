define (require)->
  
  config          = require '../config'
  TripModel       = require '../models/trip_model'
  SavedTripModel  = require '../models/saved_trip_model'
  UsersCollection = require '../collections/users_collection'
    
  class AddTripView extends Backbone.View
    el: $ '#addtrip'

    template: _.template $('#addtrip-template').html()

    initialize: (options) ->
      @render()

    events:
      # 'changeDate .date':       'renderFriendlyDate'
      'change #cost':           'updateConvertedRate'
      'change #curr':           'updateConvertedRate'
      'submit #add-trip-form':  'saveTrip'

    render: =>
      @$el.html @.template()

      $('#depart-date').data('date', Date.create().format("{M}/{dd}/{yyyy}"))
      $('#return-date').data('date', Date.create().addDays(7).format("{M}/{dd}/{yyyy}"))

      $('div.date').datepicker()

      $('.place.from').typeahead
        property: "origin"
        source: (typeahead, query) =>
          console.log 1, @
          return if query.length < 3
          window.typeahead = typeahead;
          @getPlace(query)

      $('.place.to').typeahead
        property: "destination"
        source: (typeahead, query) =>
          return if query.length < 3
          window.typeahead = typeahead;
          @getPlace(query)

      $('#assignee').typeahead
        # property: "assignee"
        source: (typeahead, query) =>
          # _.map people.items, (o) ->
          #   o.label
          return if query.length < 3
          new UsersCollection
            q: query
            callback: (d) =>
              # console.log 3,d,_.pluck(d, 'name')
              typeahead.process _.pluck(d, 'name')

      $('#cost, #curr').on('change keyup', =>)
      @

    getRate: (from, to) ->
      $('#currscript').remove()
      script = document.createElement 'script'
      script.setAttribute 'src', 
        "http://query.yahooapis.com/v1/public/yql?q=select%20rate%2Cname%20from%20csv%20where%20url%3D'http%3A%2F%2Fdownload.finance.yahoo.com%2Fd%2Fquotes%3Fs%3D#{from}#{to}%253DX%26f%3Dl1n'%20and%20columns%3D'rate%2Cname'&format=json&callback=parseExchangeRate"
      script.setAttribute 'id', 'currscript'
      document.body.appendChild script

    updateConvertedRate: ->
      @getRate $('#curr').val(), 'AUD'

    getPlace: (q) ->
      $('#placescript').remove()
      script = document.createElement 'script'
      script.setAttribute 'src',
        "http://gd.geobytes.com/AutoCompleteCity?q=#{q}&callback=handlePlaceJSONP"
      script.setAttribute 'id', 'placescript'
      document.body.appendChild script

    focus: ->
      focusCb = => @$el.find("input[type='text']:first").focus()
      setTimeout focusCb, 0

    showInvalid: (model, errs) =>
      for attr of errs
        @$el.find('.'+attr).addClass 'error'
      false

    showSavedToast: ->
      @flash = new FlashView
        flash: 
          title:    'Success'
          message:  'Your travel approval request has been saved and sent for approval'

    saveTrip: ->
      @$('.error').removeClass 'error'    

      trip = $('form').serializeObject()
      trip[config.customFields.departDate.id] = Date.create($('#depart-date input').data('date')).format(Date.ISO8601_DATETIME)
      trip[config.customFields.returnDate.id] = Date.create($('#return-date input').data('date')).format(Date.ISO8601_DATETIME)

      tripObj =
        fields:
          issuetype:
            id: issuetype.id
          project:
            id: project.id

      _.extend tripObj.fields,
        summary: trip.summary if trip.summary
      _.extend tripObj.fields,
        description: trip.description if trip.description
      tripObj.fields[config.customFields.origin.id] = trip.origin if trip.origin
      tripObj.fields[config.customFields.destination.id] = trip.destination if trip.destination
      tripObj.fields[config.customFields.departDate.id] = trip[config.customFields.departDate.id] if trip[config.customFields.departDate.id]
      tripObj.fields[config.customFields.returnDate.id] = trip[config.customFields.returnDate.id] if trip[config.customFields.returnDate.id]
      tripObj.fields[config.customFields.purchaseAmt.id] = parseInt(trip.purchaseAmt) if trip.purchaseAmt

      if trip.currency
        tripObj.fields[config.customFields.currency.id] = {}
        tripObj.fields[config.customFields.currency.id]['value'] = trip.currency

      _.extend tripObj.fields, if trip.assignee
        assignee:
            # name: trip.assignee
            name: "alex"
      @model = new TripModel
      @model.on 'error', @showInvalid
      @model.set trip
      if @model.errors.length == 0 
        rslt = @model.save tripObj,
          success: (model, d) =>
            @render()
            @showSavedToast()
            console.log 'success', @, arguments
            savedTrip = new SavedTripModel
              key: d.key
              callback: (trip) =>
                console.log 'callback', savedTrip
                @collection.add savedTrip
          error: (model, errs) =>
            # @showInvalid model, errs

      false