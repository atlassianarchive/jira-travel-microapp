define ->
  
  class MyTripView extends Backbone.View
    template: _.template $('#mytrip-template').html()

    tagName: 'tr'
    
    initialize: (options) ->
      @model.bind 'change', @render
      @model.bind 'destroy', @remove

    render: ->
      @$el.html @template(@model) if @model

    remove: ->
      @$el.remove()