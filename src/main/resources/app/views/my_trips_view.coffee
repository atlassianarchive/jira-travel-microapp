define (require)->

  MyTripView = require './my_trip_view'
    
  class MyTripsView extends Backbone.View
    el: $ '#mytrips'

    template: _.template $('#mytrips-template').html()

    initialize: (options) ->
      @collection.on 'add', @addOne
      @collection.on 'change', @render, @
      @render()

    render: ->
      @$el.html @template()
      @addAll()

    addOne: (model) =>
      myTripView = new MyTripView({model: model})
      myTripView.render();
      @$el.find('#mytrips-rows').prepend myTripView.el
      model.bind 'remove', myTripView.remove

    addAll: ->
      @collection.each @addOne

